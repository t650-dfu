
# GCC before 5 has a bug with -Wmissing-field-initializers
#CC ?= clang
WFLAGS ?= -Wall -Wextra
CFLAGS ?= -I../lib -g
LDFLAGS ?= -L../lib -lltunify

fw-update: fw-update.c ../lib/libltunify.a
	$(CC) $(WFLAGS) $(CFLAGS) $< -o $@ $(LDFLAGS)
