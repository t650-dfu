# Logitech Unifying firmware updater for Linux
This repository contains an experimental firmware updater for the Logitech
Wireless Rechargeable Touchpad T650. It was successfully tested on Lekensteyn's
T650 device.

The tool is created using reverse engineering on the [official Logitech firmware
updater][1] and a virtual, fake [Logitech Unifying Receiver device in QEMU][2].
It may therefore not work in certain edge cases although I have tried my best to
match the behavior of the firmware updater. See also the comments in
`fw-update.c`. The custom QEMU sources are based on v2.2.0, with
v2.2.0-76-g5ac86ef being the latest version which was used for REing ("unifying:
acknowledge device when entering LTx").

If the DFU (Device Firmware Updater) process is generic to all Logitech devices,
then this tool could be modified to handle other devices as well. As of now
(December 2014), it contains some hard-coded details.

Feel free to risk your device by trying this tool, it worked for me, but YMMV.

## Building
This tool depends on an experimental ltunify library. The expected directory
layout is:

 - ltunify: https://git.lekensteyn.nl/ltunify.git, branch refactor-lib, tested
   with commit 1b3247bc573314e4b05ce5bc60cf9f14c93228a5
 - ltunify/t650-dfu: this repository, tested with commit
   4cc4c75708811d8ef54edd2384b14d00b736c874

    cd ltunify/lib
    make
    cd ../t650-dfu
    make

This should produce a `fw-update` binary (tested with Clang 3.5.0).

## Usage
The command is used as follows:

    ./fw-update /dev/hidrawX device_index flash-file

Example for my T650 device which is paired on index 2 (use `ltunify list` to
find out the index):

    ./fw-update /dev/hidraw1 2 RQM_041_001_00038.bin

## Copyright
The `RQM_041_001_00038.bin` firmware blob is from Logitech. License unknown, but
it was retrieved from USB communications between the firmware updater and the
virtual USB device.

The `fw-update.c` is authored by Peter Wu (peter@lekensteyn.nl). It is released
under the terms of the GPL license version 3 (or any latter version, at your
choice).

 [1]: http://www.logitech.com/en-us/support/articles/unifying-firmware-t650
 [2]: https://git.lekensteyn.nl/peter/qemu/log/?h=logitech-unifying-2.2
